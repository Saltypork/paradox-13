
=begin

もんむす・くえすと！ＲＰＧ
　トリス修正B  ver3  2014/11/17



機能一覧　原因と解決法は下　統合済みのものは×
×バグ修正：捕食された後のメンバー入れ替え（優先度・中）
×要望修正：捕食技で倒した敵の経験値がもらえない（優先度・低）
×独自修正：メンバー入れ替えと捕食後戦闘終了の並び替えによる全滅を防ぐ
×独自修正：仲間想いの補正率を「スキル対象者の敵対勢力」から計算していたのを修正



機能　原因と解決法
×バグ修正：捕食された後のメンバー入れ替え（優先度・中）
　　　原因：捕食されると「パーティの並び順はそのまま」で「隠れ状態」となる
　　　　　　入れ替えはパーティの「上窓選択」番と「４＋下窓選択」番を入れ替える
　　　解決：入れ替えの直前に「捕食されたアクター」の並び順を最後尾に移動し整理

×要望修正：捕食技で倒した敵の経験値がもらえない（優先度・低）
　　　原因：「隠れ状態」のバトラーは「戦闘不能メンバー」の一覧に現れない
　　　解決：経験値などのみ「戦闘不能メンバーと捕食メンバー」の一覧を使うように

×独自修正：メンバー入れ替えと捕食後戦闘終了の並び替えによる全滅を防ぐ
　　　原因：捕食は「捕食されたアクター」が戦闘終了で元の位置に戻るため
　　　　　　メンバー入れ替えは戦闘不能アクターも自由に入れ替え可能なため
　　　解決：捕食は元の位置に戻ると全滅となる場合、捕食アクターは最後尾に入れる
　　　　　　メンバー入れ替えは、全滅状態になるような入れ替えは選択不可

×独自修正：仲間想いの補正率を「スキル対象者の敵対勢力」から計算していたのを修正
　　　原因：省略
　　　解決：「スキル使用者の仲間勢力」の戦闘不能者数から計算するようにした


=end