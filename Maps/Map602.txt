﻿


EVENT   1
 PAGE   1
  0()



EVENT   2
 PAGE   1
  0()



EVENT   3
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<???>Kagetsumugi... how was the unblessed hero?")
  205(2,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x46,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x0e,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x09,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x09,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x15,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
  ShowMessageFace("kagetumugi_fc1",4,0,2,2)
  ShowMessage("\\n<Kagetsumugi>I wonder... neither fish nor fowl. I'm unable to judge him.")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("\\n<???>Oh, what an indifferent answer...")
  ShowMessageFace("kagetumugi_fc1",4,0,2,4)
  ShowMessage("\\n<Kagetsumugi>My evaluation is probably similar. To you, I'm also neither fish nor fowl...")
  ScrollMap(8,5,4)
  ShowMessageFace("kagetumugi_fc1",4,0,2,5)
  ShowMessage("\\n<Kagetsumugi>...isn't that right? Your majesty, Alice the 8th?")
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0e,0x6b,0x75,0x72,0x6f,0x61,0x6c,0x69,0x63,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  PictureDisplay(5,"80_alice8th_st01",0,0,0,0,100,100,0,0)
  PictureMove(5,null,0,0,0,0,100,100,255,0,30,true)
  ShowMessageFace("alice8th_fc1",0,0,2,6)
  ShowMessage("\\n<Black Alice>I'm evaluating you highly... the last puppeteer, the 4th Kagetsumugi.")
  PictureDisplay(6,"80_kagetumugi_st51",0,0,-150,0,100,100,255,0)
  PictureDisplay(5,"80_alice8th_st01",0,0,150,0,100,100,255,0)
  ShowMessageFace("kagetumugi_fc1",0,0,2,7)
  ShowMessage("\\n<Kagetsumugi>Your majesty is the one who titled the first Kagetsumugi 500 years ago. And now, the puppeteer techniques have been completed and the end is coming.")
  ShowMessageFace("kagetumugi_fc1",0,0,2,8)
  ShowMessage("\\n<Kagetsumugi>The ultimate puppeteer techniques the 4th Kagetsumugi has devised... will surely become your majesty's power.")
  ShowMessageFace("alice8th_fc1",0,0,2,9)
  ShowMessage("\\n<Black Alice>I'll be relying on you, after all, my dream was destroyed twice now. Continuing to fight despite losing every time is also amusing...")
  ShowMessageFace("alice8th_fc1",0,0,2,10)
  ShowMessage("\\n<Black Alice>Even with the power of the elixir fusing the holy and dark energies, the 'White Rabbit'... I was unable to win against the unblessed hero...")
  ShowMessageFace("kagetumugi_fc1",0,0,2,11)
  ShowMessage("\\n<Kagetsumugi>To think that boy would be that powerful... honestly, I can't imagine that.")
  ShowMessageFace("alice8th_fc1",0,0,2,12)
  ShowMessage("\\n<Black Alice>This time let's go with a small but elite party. Let's pull mythic-class monsters with us, the ones who reigned during the Great Monster Wars era...")
  PictureClear(5)
  PictureClear(6)
  Wait(60)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0e,0x6d,0x6f,0x6e,0x5f,0x66,0x6c,0x61,0x73,0x68,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69))
  PictureDisplay(10,"sys_white",0,0,0,0,100,100,0,0)
  PictureMove(10,null,0,0,0,0,100,100,255,0,30,true)
  ChangeSwitch(11,11,0)
  PictureMove(10,null,0,0,0,0,100,100,0,0,30,true)
  PictureClear(10)
  Wait(60)
  PictureDisplay(5,"80_azidahaka_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("azidahaka_fc1",0,0,2,13)
  ShowMessage("\\n<Azi Dahaka>We just have to destroy this world, right...")
  PictureDisplay(5,"80_seitentaisei_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("seitentaisei_fc1",0,0,2,14)
  ShowMessage("\\n<Sun Wukong>Leave it to me, I'll rampage all I please!")
  PictureDisplay(5,"80_coatlus_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("coatlus_fc1",0,0,2,15)
  ShowMessage("\\n<Quetzalcoatl>Fragile current era monsters are no opponents for me...")
  PictureDisplay(5,"80_tezcatlipoca_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("tezcatlipoca_fc1",0,0,2,16)
  ShowMessage("\\n<Tezcatlipoca>Unyahh! Leave it to my poweer nyah!")
  PictureDisplay(5,"80_izanami_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("izanami_fc1",0,0,2,17)
  ShowMessage("\\n<Izanami>I shall send everything back to underworld...")
  PictureDisplay(5,"80_tukumokomati_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("tukumokomati_fc1",0,0,2,18)
  ShowMessage("\\n<Himiko>Kuku... lookin' like it'll be a showy feast.")
  PictureDisplay(6,"80_kagetumugi_st51",0,0,-150,0,100,100,255,0)
  PictureDisplay(5,"80_alice8th_st01",0,0,150,0,100,100,255,0)
  ShowMessageFace("kagetumugi_fc1",0,0,2,19)
  ShowMessage("\\n<Kagetsumugi>Fufu, what reliable mythical VIPs... well then, I'll add the Monster Lord Puppets I have made.")
  PictureDisplay(5,"80_alice9th_st01",0,0,0,0,100,100,255,0)
  PictureClear(6)
  ShowMessageFace("alice9th_fc1",0,0,2,20)
  ShowMessage("\\n<Alipheese the 9th>O-Onee-sama... g-good e... evening...")
  ShowMessageFace("alice8th_fc1",0,0,2,21)
  ShowMessage("\\n<Black Alice>Ufufu, those are zombie techniques that won't lose to those of Ariste's. This is truly the ultimate fusion of zombie and puppeteering techniques.")
  ShowMessageFace("alice8th_fc1",1,0,2,22)
  ShowMessage("\\n<Black Alice>Even my little sister who opposed me so much has become so obedient... it looks like we'll be able to get along and play with puppets together now.")
  PictureDisplay(5,"80_alice11th12th_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("alice11th12th_fc2",0,0,2,23)
  ShowMessage("\\n<Alipheese the 11+12th>Ufufu... Ahaha...")
  ShowMessageFace("kagetumugi_fc1",0,0,2,24)
  ShowMessage("\\n<Kagetsumugi>The 11th and the 12th had many parts missing from their bodies so... I fused their bodies together and succeeded in creating one.")
  ShowMessageFace("alice8th_fc1",0,0,2,25)
  ShowMessage("\\n<Black Alice>Ufufu, looks wonderful. I love this sense of yours...")
  PictureDisplay(5,"80_alice6th_st01",0,0,0,0,100,100,255,0)
  ShowMessageFace("alice6th_fc1",0,0,2,26)
  ShowMessage("\\n<Alipheese the 6th>Stand...back... I will be...Monster Lord... Alicepheese...")
  ShowMessageFace("alice8th_fc1",0,0,2,27)
  ShowMessage("\\n<Black Alice>My, if it isn't my dear aunt. You look in good health...")
  ShowMessageFace("kagetumugi_fc1",0,0,2,28)
  ShowMessage("\\n<Kagetsumugi>Her body was enshrined in Yamatai... and so there was few damage and I was able to finish her quickly.")
  ShowMessageFace("kagetumugi_fc1",0,0,2,29)
  ShowMessage("\\n<Kagetsumugi>You will have to wait for other Monster Lord Puppets. Although I gathered their bodies, they are heavily damaged...")
  PictureDisplay(6,"80_kagetumugi_st51",0,0,-150,0,100,100,255,0)
  PictureDisplay(5,"80_alice8th_st01",0,0,150,0,100,100,255,0)
  ShowMessageFace("alice8th_fc1",0,0,2,30)
  ShowMessage("\\n<Black Alice>With this much forces we'll surely start an enjoyable tea party. Let's enjoy ourselves plenty, ufufu.")
  ShowMessageFace("kagetumugi_fc1",0,0,2,31)
  ShowMessage("\\n<Kagetsumugi>Well then, for your majesty's third time's glory...in order to make the world yours...")
  ShowMessageFace("alice8th_fc1",0,0,2,32)
  ShowMessage("\\n<Black Alice>In order to make the entire world mine... Ufufufufu...")
  221()
  ChangeSwitch(11,11,1)
  PictureClear(5)
  PictureClear(6)
  242(1)
  Wait(60)
  211(1)
  TeleportPlayer(0,2,188,294,2,2)
  222()
  355("gain_medal(25)")
  0()



EVENT   4
 PAGE   1
  // condition: switch 11 is ON
  0()



EVENT   5
 PAGE   1
  // condition: switch 11 is ON
  0()



EVENT   6
 PAGE   1
  // condition: switch 11 is ON
  0()



EVENT   7
 PAGE   1
  // condition: switch 11 is ON
  0()



EVENT   8
 PAGE   1
  // condition: switch 11 is ON
  0()



EVENT   9
 PAGE   1
  // condition: switch 11 is ON
  0()
